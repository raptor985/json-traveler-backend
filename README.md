# JSON traveler backend

To build, use:
```
gradlew clean shadowJar
```

To run:
```
java -jar build/libs/json-traveler-backend-0.0.1-all.jar
```

Also, the app supports some additional parameters:
* app.jsonDir - Directory with JSON files to scan. Default: ./jsons/
* app.maxFileSize - Maximum size of JSON file that can be loaded by app (bytes). Default: 67108864

Which can be passed in the following way:
```
java -jar build/libs/json-traveler-backend-0.0.1-all.jar -P:app.jsonDir="my/json/dir/path>" 
```
