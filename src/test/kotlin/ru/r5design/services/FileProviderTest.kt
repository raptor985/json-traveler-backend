package ru.r5design.services

import org.junit.*
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.util.concurrent.ThreadLocalRandom

internal class FileProviderTest {
    private lateinit var dir: File
    private lateinit var subject: FileProvider

    private fun File.touch() {
        if (!this.exists()) {
            FileOutputStream(this).close();
        }

        this.setLastModified(System.currentTimeMillis());
    }

    @Before
    fun setUp() {
        dir = Files.createTempDirectory("jsons").toFile()
    }

    @After
    fun tearDown() {
        subject.close()
        dir.delete()
    }

    @Test
    fun `test that provider sees existing files`() {
        sequenceOf("a", "b")
            .forEach { dir.resolve("$it.json").touch() }
        subject = FileProvider(dir.toPath()) { error("no changes expected") }
        Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*.json").toSet())
        Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*").toSet())
        Assert.assertEquals(listOf("a.json"), subject.findByWildcard("a*"))
        Assert.assertEquals(listOf("b.json"), subject.findByWildcard("b*.json"))
    }

    @Test
    fun `test that provider sees newly created files`() {
        dir.resolve("a.json").touch()
        subject = FileProvider(dir.toPath()) { error("no changes expected") }
        Assert.assertEquals(listOf("a.json"), subject.findByWildcard("*.json"))
        dir.resolve("b.json").touch()
        Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*").toSet())
    }

    @Test
    fun `test that provider forgets about deleted files`() {
        sequenceOf("a", "b")
            .forEach { dir.resolve("$it.json").touch() }
        val changeHookTriggeredFor = mutableSetOf<String>()
        subject = FileProvider(dir.toPath()) { changeHookTriggeredFor.add(it) }
        Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*").toSet())
        dir.resolve("b.json").delete()
        repeat(100) {
            val files = subject.findByWildcard("*.json")
            if (files.size == 1) {
                Assert.assertEquals(listOf("a.json"), subject.findByWildcard("*.json"))
                Assert.assertEquals(setOf("b.json"), changeHookTriggeredFor)
                return
            }
            Thread.sleep(100)
        }
        throw AssertionError("File deletion hook was not triggered")
    }

    @Test
    fun `test that provider caches file updates`() {
        sequenceOf("a", "b")
            .forEach { dir.resolve("$it.json").touch() }
        val changeHookTriggeredFor = mutableSetOf<String>()
        subject = FileProvider(dir.toPath()) { changeHookTriggeredFor.add(it) }
        Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*").toSet())
        dir.resolve("b.json").writeBytes(ByteArray(8).apply {
            ThreadLocalRandom.current ().nextBytes(this)
        })
        repeat(100) {
            if (changeHookTriggeredFor.size == 1) {
                Assert.assertEquals(setOf("b.json"), changeHookTriggeredFor)
                Assert.assertEquals(setOf("a.json", "b.json"), subject.findByWildcard("*.json").toSet())
                return
            }
            val files = subject.findByWildcard("*.json")
            Thread.sleep(100)
        }
        throw AssertionError("File deletion hook was not triggered")
    }
}