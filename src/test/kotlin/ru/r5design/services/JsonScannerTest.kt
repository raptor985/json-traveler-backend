package ru.r5design.services

import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.spi.json.JsonProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.*
import ru.r5design.exception.TooBigFileException
import java.io.File
import java.nio.file.Files

@InjectMockKs
internal class JsonScannerTest {
    private val dir: File = Files.createTempDirectory("jsons").toFile()
    private lateinit var subject: JsonScanner

    @MockK
    lateinit var jsonProvider: JsonProvider

    init {
        dir.resolve("example.json").writeBytes(
            this.javaClass.classLoader.getResourceAsStream("example.json")!!.readAllBytes()
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        subject.close()
    }

    @Test(expected = TooBigFileException::class)
    fun `test that scanner doesn't try to fit too big file in memory`() {
        val size = dir.resolve("example.json").length()
        subject = JsonScanner(
            dir.absolutePath,
            size - 1,
            jsonProvider
        )
        runBlocking {
            subject.selectData("example.json", "")
        }
    }

    @Test
    fun `test that scanner selects data from file`() {
        val size = dir.resolve("example.json").length()
        subject = JsonScanner(
            dir.absolutePath,
            size,
            Configuration.defaultConfiguration().jsonProvider()
        )
        val result = runBlocking {
            subject.selectData("example.json", "\$.products.*[?('web' in @.tags)]")
        }
        val expected = listOf(
            linkedMapOf(
                "description" to "Product A1",
                "tags" to listOf(
                    "web", ".net"
                ),
                "forSale" to true
            )
        )
        Assert.assertEquals(expected, result)
    }


}