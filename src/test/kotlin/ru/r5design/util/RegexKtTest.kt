package ru.r5design.util

import org.junit.Assert
import org.junit.Test

internal class RegexKtTest {
    @Test
    fun `test asWildcardRegex no asterisk`() {
        val regex = "example.json".asWildcardRegex()
        Assert.assertTrue(regex.matches("example.json"))
        Assert.assertFalse(regex.matches("example2.json"))
    }

    @Test
    fun `test asWildcardRegex with asterisk`() {
        val regex = "example*.json".asWildcardRegex()
        Assert.assertTrue(regex.matches("example.json"))
        Assert.assertTrue(regex.matches("example2.json"))
        Assert.assertTrue(regex.matches("example.new.json"))
        Assert.assertFalse(regex.matches("exampl.json"))
    }
}