package ru.r5design.util

import org.junit.Assert
import org.junit.Test

internal class AccessRememberingMapTest {

    @Test
    fun `test no entries`() {
        val map = AccessRememberingMap<String, Int>()
        Assert.assertEquals(listOf<Int>(), map.values().toList())
        Assert.assertNull(map.oldestKey())
    }

    @Test
    fun `test one entry`() {
        val map = AccessRememberingMap<String, Int>()
        map.put("a", 1)
        Assert.assertEquals(listOf(1), map.values().toList())
        Assert.assertEquals("a", map.oldestKey())
        map.remove("a")
        Assert.assertEquals(listOf<Int>(), map.values().toList())
    }

    @Test
    fun `test multiple entries`() {
        val map = AccessRememberingMap<String, Int>()
        map.put("a", 1)
        map.put("b", 2)
        map.put("c", 3)
        Assert.assertEquals(setOf(1, 2, 3), map.values().toSet())
        Assert.assertEquals("a", map.oldestKey())
        map.remove("a")
        map.remove("c")
        Assert.assertEquals(listOf(2), map.values().toList())
    }

    @Test
    fun `test multiple entries last access`() {
        val map = AccessRememberingMap<String, Int>()
        map.put("a", 1)
        map.put("b", 2)
        map.put("c", 3)
        Assert.assertEquals("a", map.oldestKey())
        map["a"]
        Assert.assertEquals("b", map.oldestKey())
        map["b"]
        Assert.assertEquals("c", map.oldestKey())
        map["c"]
        Assert.assertEquals("a", map.oldestKey())
    }
}