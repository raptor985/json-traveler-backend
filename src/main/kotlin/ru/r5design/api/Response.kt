package ru.r5design.api

/**
 * Success response of application endpoint
 *
 * @property data result of the completed request
 */
data class Response<T>(
    val data: T
)