package ru.r5design.api

/**
 * Error response of application endpoint
 *
 * @property message message that should be displayed to user
 */
data class ErrorResponse(
    val message: String
)