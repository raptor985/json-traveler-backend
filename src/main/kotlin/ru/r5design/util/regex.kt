package ru.r5design.util

/**
 * Makes regex which matches specified wildcard. All characters, except '*' are taken as-is. * means
 * "any characters, 0 or more".
 *
 * @receiver wildcard string
 * @return regex, matching the wildcard
 */
fun String.asWildcardRegex() = Regex(
        this.split("*")
                .asSequence()
                .map { Regex.escape(it) }
                .joinToString(".*")
)