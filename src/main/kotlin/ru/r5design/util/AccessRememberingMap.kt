package ru.r5design.util

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

/**
 * Map, which remembers time of access to its entries
 */
class AccessRememberingMap<K: Any, V> {
    private val data = ConcurrentHashMap<K, DataWithAccessTime<V>>()
    private val counter = AtomicLong()

    /**
     * @see Map.get
     */
    operator fun get(key: K): V? = data[key]?.run {
        this.lastAccess = accessOrder()
        this.data
    }

    /**
     * @see MutableMap.put
     */
    fun put(key: K, value: V): V? = data.put(
        key,
        DataWithAccessTime(value, accessOrder())
    )?.data

    /**
     * @see MutableMap.remove
     */
    fun remove(key: K): V? = data.remove(key)?.data

    /**
     * @return sequence of values of the map
     */
    fun values(): Sequence<V> = data.values.asSequence().map { it.data }

    /**
     * @return key, which value was accessed the most further in past. null - if the map is empty.
     */
    fun oldestKey(): K? {
        var key: K? = null
        var accessed: Long = Long.MAX_VALUE
        data.forEach { (k, v) ->
            if (v.lastAccess < accessed) {
                accessed = v.lastAccess
                key = k
            }
        }
        return key
    }

    /**
     * @return next value of entry access order. The less value - the earlier the entry was accessed.
     */
    private fun accessOrder(): Long = counter.incrementAndGet()

    private class DataWithAccessTime<V>(
        val data: V,
        @Volatile var lastAccess: Long
    )
}