package ru.r5design.util

import org.slf4j.Logger
import org.slf4j.event.Level

/**
 * Logs message at specified level
 *
 * @param level log level
 * @param message message text
 * @param cause cause which should be passed to logger
 */
fun Logger.log(level: Level, message: String, cause: Throwable? = null) {
    when (level) {
        Level.ERROR -> this.error(message, cause)
        Level.WARN -> this.warn(message, cause)
        Level.INFO -> this.info(message, cause)
        Level.DEBUG -> this.debug(message, cause)
        Level.TRACE -> this.trace(message, cause)
    }
}