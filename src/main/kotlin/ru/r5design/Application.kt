package ru.r5design

import io.ktor.features.*
import io.ktor.jackson.*
import com.fasterxml.jackson.databind.*
import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.spi.json.JsonProvider
import io.ktor.application.*
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.ktor.DIFeature
import org.kodein.di.singleton
import ru.r5design.services.JsonScanner

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    val config = this.environment.config
    install(DIFeature) {
        bind<JsonProvider>() with singleton {
            Configuration.defaultConfiguration().jsonProvider()
        }
        bind<JsonScanner>() with singleton { JsonScanner(
            config.property("app.jsonDir").getString(),
            config.property("app.maxFileSize").getString().toLong(),
            instance()
        ) }
    }

    install(AutoHeadResponse)
    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    registerRoutes()
}
