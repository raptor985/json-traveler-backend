package ru.r5design

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance
import org.kodein.di.ktor.di
import org.slf4j.LoggerFactory
import ru.r5design.api.ErrorResponse
import ru.r5design.api.Response
import ru.r5design.exception.AppException
import ru.r5design.exception.RequestParamNotSetException
import ru.r5design.services.JsonScanner
import ru.r5design.util.log

fun Application.registerRoutes() {
    val jsonProvider: JsonScanner by di().instance()

    routing {
        install(StatusPages) {
            val log = LoggerFactory.getLogger("Request exception handler")
            exception<AppException> { cause ->
                log.log(cause.logLevel, cause.message, cause.cause)
                call.respond(HttpStatusCode.BadRequest, ErrorResponse(cause.publicMessage))
            }
            exception<Exception> { cause ->
                log.error("Unhandled exception caught, while processing request", cause.cause)
                call.respond(HttpStatusCode.InternalServerError, ErrorResponse("Internal error"))
            }
        }

        get("/") {
            call.respondText("JSONPath query server")
        }

        get("/jscan") {
            val query = call.parameters["query"] ?: throw RequestParamNotSetException("query")
            val fileMask = call.parameters["files"] ?: throw RequestParamNotSetException("files")

            call.respond(Response(jsonProvider.findDataInFiles(fileMask, query)))
        }
    }
}