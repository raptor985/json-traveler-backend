package ru.r5design.services

import org.slf4j.LoggerFactory
import ru.r5design.util.asWildcardRegex
import java.io.Closeable
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * This service watches for changes in specified directory (non-recursive), providing capability to select files by mask
 * and run callbacks in case of file changes.
 *
 * @param directory directory, on which files watcher works
 * @param onChange callback for file updated/deleted events
 */
class FileProvider(
    private val directory: Path,
    private val onChange: (String) -> Unit
) : Closeable {
    private val watchExecutor = Executors.newSingleThreadExecutor()
    private val watchService = FileSystems.getDefault().newWatchService()
    private val availableFiles: MutableSet<String> = ConcurrentHashMap.newKeySet()

    init {
        // Starting directory events watcher
        watchExecutor.submit {
            try {
                directory.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.OVERFLOW
                )
                while (true) {
                    if (watchExecutor.isShutdown || Thread.currentThread().isInterrupted) {
                        return@submit
                    }
                    val key = watchService.poll(1, TimeUnit.SECONDS) ?: continue
                    key.pollEvents().forEach { event ->
                        // ENTRY_CREATE/DELETE/MODIFY events have Path object as context
                        val path = event.context() as? Path ?: return@forEach
                        val filename = path.fileName.toString()
                        when (event.kind()) {
                            StandardWatchEventKinds.ENTRY_CREATE -> {
                                availableFiles.add(filename)
                                log.info("File {} created", filename)
                            }
                            StandardWatchEventKinds.ENTRY_DELETE -> {
                                availableFiles.remove(filename)
                                onChange(filename)
                                log.info("File {} deleted", filename)
                            }
                            StandardWatchEventKinds.ENTRY_MODIFY -> {
                                onChange(filename)
                                log.info("File {} updated", filename)
                            }
                            StandardWatchEventKinds.OVERFLOW -> {
                                error("Unexpected overflow of watch service events")
                            }
                        }
                    }
                    key.reset()
                }
            } catch (e: InterruptedException) {
                throw e
            } catch (e: Exception) {
                log.error("Caught an error while watching for filesystem changes", e)
            }
        }

        // Discovering initial fileset
        (directory.toFile().listFiles() ?: error("${directory.toAbsolutePath()} is not a directory"))
            .asSequence()
            .filter { it.isFile }
            .forEach {
                availableFiles.add(it.name)
                log.info("File {} found", it.name)
            }
    }

    /**
     * Resolves specified filename with base dir of this provider
     *
     * @return null if file is not in the list of existing files of base dir.
     * Otherwise - the file with specified name in the base dir.
     */
    fun resolve(filename: String) = if (availableFiles.contains(filename)) {
        directory.resolve(filename).toFile()
    } else {
        null
    }

    /**
     * Finds all names of files, currently existing in base dir, matching the specified wildcard
     *
     * @param nameWildcard wildcard (accepting * character) to filter filenames from provider's dir
     * @return list of filenames
     */
    fun findByWildcard(nameWildcard: String): List<String> {
        val wildcardRegex = nameWildcard.asWildcardRegex()
        return availableFiles
            .asSequence()
            .filter { wildcardRegex.matches(it) }
            .toList()
    }

    override fun close() {
        watchExecutor.shutdown()
        watchExecutor.awaitTermination(1, TimeUnit.SECONDS)
        watchService.close()
    }

    companion object {
        private val log = LoggerFactory.getLogger(FileProvider::class.java)
    }
}