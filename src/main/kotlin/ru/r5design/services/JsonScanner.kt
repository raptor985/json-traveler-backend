package ru.r5design.services

import com.jayway.jsonpath.JsonPath
import com.jayway.jsonpath.PathNotFoundException
import com.jayway.jsonpath.spi.json.JsonProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import ru.r5design.exception.IncorrectJsonPathException
import ru.r5design.exception.TooBigFileException
import ru.r5design.util.AccessRememberingMap
import java.io.Closeable
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Paths

/**
 * Service, providing an ability to perform JSONPath requests against files in particular directory.
 * Caches parsed files to reduce disk reads for the most common used files
 */
class JsonScanner(
    jsonDir: String,
    private val maxFileSize: Long,
    private val jsonParser: JsonProvider
) : Closeable {
    private val fileReadLock = Mutex()
    private val cachedJsons = AccessRememberingMap<String, CachedJson>()

    private val files: FileProvider = FileProvider(
        Paths.get(jsonDir),
        onChange = {
            cachedJsons.remove(it)
        }
    )

    /**
     * Selects data by JSONPath expression from specified file
     *
     * @param filename file, in which we perform search
     * @param jsonPath JSONPath expression
     * @return JSON objects (presented by simple types and maps/lists) with selection results.
     *  null - if the file doesn't exist anymore
     */
    suspend fun selectData(filename: String, jsonPath: String): Any? {
        val content = jsonNodeFrom(filename) ?: return null
        val path = try {
            JsonPath.compile(jsonPath)
        } catch (e: Exception) {
            throw IncorrectJsonPathException(e, jsonPath)
        }
        return try {
            path.read<Any?>(content)
        } catch (e: PathNotFoundException) {
            return null
        }
    }

    /**
     * Selects data by JSONPath expression from files by specified wildcard
     *
     * @param filename file, in which we perform search
     * @param jsonPath JSONPath expression
     * @return Map of (filename : selected data). Contains only files for which any data had been selected by
     * JSONPath query.
     */
    suspend fun findDataInFiles(fileMask: String, jsonPath: String): Map<String, Any> {
        return files.findByWildcard(fileMask)
            .map { it to selectData(it, jsonPath) }
            .filter { (_, data) -> data != null }
            .toMap() as Map<String, Any>    // We can cast Any? to Any because of the filter above
    }

    override fun close() {
        files.close()
    }

    /**
     * Removes parsed JSON files from cache to free enough space to cache contents of the new file.
     * Tries to remove accessed earlier (and, probably, more rare used) cache entries.
     * <b>Not thread-safe.</b>
     *
     * @throws TooBigFileException in case if we can't fit the file in memory, due to specified file size restrictions
     * @return size of the new file
     */
    private fun freeCacheForFileContent(file: File): Long {
        val size = file.length()
        if (size > maxFileSize) {
            throw TooBigFileException(file.name, size, maxFileSize)
        }
        var usedSize = cachedJsons.values().map { it.size }.sum()
        while (maxFileSize - usedSize < size) {
            val oldest = cachedJsons.oldestKey()!! // We always have at least 1 key at this point because size < max
            cachedJsons.remove(oldest)
            log.trace("File {} removed from cache")
            usedSize = cachedJsons.values().map { it.size }.sum()
        }
        return size
    }


    /**
     * Parses the whole file specified to JSON object.
     *
     * @param filename file in JSON files directory, which we wanna read
     * @return JSON object parsed from the file's contents. null - if file does not exist
     */
    private suspend fun jsonNodeFrom(filename: String): Any? {
        var cached = cachedJsons[filename]?.data
        if (cached != null) {
            return cached
        }
        return fileReadLock.withLock {
            cached = cachedJsons[filename]?.data
            if (cached != null) {
                // While we was waiting for the lock, someone else pushed content of the file to the cache
                return cached
            }
            // While we was waiting for the lock, the file may be deleted (return null)
            val file = files.resolve(filename) ?: return null
            val fileSize = freeCacheForFileContent(file)
            val added = CachedJson(
                withContext(Dispatchers.IO) {
                    jsonParser.parse(file.inputStream(), StandardCharsets.UTF_8.name())
                },
                fileSize
            )
            cachedJsons.put(filename, added)
            log.trace("File {} added to cache", filename)
            added.data
        }
    }

    private data class CachedJson(
        val data: Any,
        val size: Long
    )

    companion object {
        private val log = LoggerFactory.getLogger(JsonScanner::class.java)
    }

}