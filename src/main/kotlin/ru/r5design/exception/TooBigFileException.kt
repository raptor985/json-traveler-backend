package ru.r5design.exception

/**
 * Application can't load file because its size is bigger than the specified maximum
 */
class TooBigFileException(name: String, size: Long, maxSize: Long) : AppException(
    "Size of the file '$name' is too big to handle ($size bytes). Max size - $maxSize bytes."
)