package ru.r5design.exception

import org.slf4j.event.Level

/**
 * Exception with text that can be returned to user by API
 *
 * @property publicMessage message which will be sent to user as a response
 * @property message exception message for internal handling (like logging)
 * @property logLevel level, with which the exception will be logged when caught
 */
abstract class AppException(
    val publicMessage: String,
    override val message: String = publicMessage,
    override val cause: Throwable? = null,
    val logLevel: Level = Level.DEBUG
) : Exception()