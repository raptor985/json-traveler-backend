package ru.r5design.exception

/**
 * Required request parameter was not found
 */
class RequestParamNotSetException(param: String) : AppException("Request parameter '$param' was not found")