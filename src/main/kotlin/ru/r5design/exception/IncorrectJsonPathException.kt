package ru.r5design.exception

/**
 * We can't compile specified expression as JSONPath
 */
class IncorrectJsonPathException(cause: Throwable, expression: String) : AppException(
   "'$expression' is not a valid JSONPath expression", cause = cause
)